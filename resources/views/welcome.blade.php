<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="{{ mix('css/app.css') }}" rel="stylesheet">
	<title>HRMS</title>
	<script src="//unpkg.com/alpinejs" defer></script>
</head>

<body>
	<div class="h-screen" x-data="open">
		<nav class="w-full bg-gray-600">
			<div class="grid grid-cols-12">
				<div class="self-center col-span-2 px-4">
					<div class="font-mono text-2xl font-bold text-white">
						LOGO
					</div>
				</div>
				<div class="self-center col-span-10">
					<div class="flex justify-between">
						<div class="left">
							<h2>Label</h2>
						</div>
						<div class="flex flex-row items-center space-x-4">
							<div class="flex">
								<input class="px-4 rounded-l-full focus:outline-none" type="search" value="" placeholder="search...">
								{{-- <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="red">
									<path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
								</svg> --}}
								<div class="self-center py-2 pr-4 bg-white rounded-r-full" style="background-color: white">
									<svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4" fill="none" viewBox="0 0 24 24" stroke="gray">
										<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
									</svg>
								</div>
							</div>
							<div class="h-full p-4 hover:bg-gray-400">
								<svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-white-50" fill="none" viewBox="0 0 24 24" stroke="white">
									<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
								</svg>
							</div>
							<div class="h-full p-4 hover:bg-gray-400">
								<svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-gray-0" fill="none" viewBox="0 0 24 24" stroke="white">
									<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20 13V6a2 2 0 00-2-2H6a2 2 0 00-2 2v7m16 0v5a2 2 0 01-2 2H6a2 2 0 01-2-2v-5m16 0h-2.586a1 1 0 00-.707.293l-2.414 2.414a1 1 0 01-.707.293h-3.172a1 1 0 01-.707-.293l-2.414-2.414A1 1 0 006.586 13H4" />
								</svg>
							</div>
							<div class="user ">
								<img class="rounded-full max-h-10 max-w-10" src="https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8dXNlciUyMHByb2ZpbGV8ZW58MHx8MHx8&w=1000&q=80" alt="" srcset="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</nav>
		<div class="grid grid-cols-12" style="height: 95%">
			<div class="col-span-2 bg-gray-700 sidebar">
				<ul class="mt-4 space-y-4">
					<li class="flex flex-row w-full px-4 space-x-2">
						<svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="white">
							<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
							<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.488 9H15V3.512A9.025 9.025 0 0120.488 9z" />
						</svg>
						<p class="text-white">
							Dashboard
						</p>
					</li>
					<li class="flex flex-row w-full px-4 space-x-2">
						<svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="white">
							<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z" />
						  </svg>
						<p class="text-white">
							Employee
						</p>
					</li>
					<ul>
						<li>Add Employee</li>
						<li>Delete Employee</li>
					</ul>
					<li class="flex flex-row w-full px-4 space-x-2">
						<svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="white">
							<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
						  </svg>
						  <p class="text-white">
							  Clients
						  </p>
					</li>
				</ul>
			</div>
	
			<div class="col-span-10 px-4 pt-2 bg-blue-300 content">
				content
			</div>
		</div>
	</div>
</body>
</html>