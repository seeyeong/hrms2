<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
		$relationship = [
			'parent',
			'siblings',
			'spouse',
			'cousin'
		];

        return [
            'name'=>$this->faker->name(),
            'phone'=>$this->faker->phoneNumber(),
            'email'=>$this->faker->safeEmail(),
            'birthday'=>$this->faker->date(),

			'primary_name'=>$this->faker->name(),
            'primary_phone'=>$this->faker->phoneNumber(),
            'primary_relationship'=>$relationship[rand(0, 3)],
        ];
    }
}
