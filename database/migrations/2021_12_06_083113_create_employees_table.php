<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
			$table->string('name', 100);
			$table->string('phone', 20);
			$table->string('email', 50);
			$table->date('birthday');
			$table->string('address', 100)->nullable();
			$table->tinyInteger('gender')->unsigned()->nullable();
			$table->bigInteger('supervisor_id')->nullable();

			//personal info
			$table->string('passport', 100)->nullable();
			$table->date('passport_expired_at')->nullable();
			$table->string('tel', 10)->nullable();
			$table->integer('country_id')->unsigned()->nullable();
			$table->integer('religion')->unsigned()->nullable();
			$table->tinyInteger('marital_status_id')->unsigned()->nullable()->default(1);
			$table->boolean('employment_of_spouse')->nullable()->default(false);
			$table->tinyInteger('no_of_children')->unsigned()->default(0);
            

			//bank info
			$table->string('bank', 20)->nullable();
			$table->string('account', 20)->nullable();

			//emergency contact
			$table->string('primary_name', 100);
			$table->string('primary_phone', 20);
			$table->string('primary_relationship', 10);

			$table->string('secondary_name', 100)->nullable();
			$table->string('secondary_phone', 20)->nullable();
			$table->string('secondary_relationship', 10)->nullable();

			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
