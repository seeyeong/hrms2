<?php

namespace App\Http\Controllers;

use App\Models\EmployeeFamily;
use Illuminate\Http\Request;

class EmployeeFamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeFamily  $employeeFamily
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeFamily $employeeFamily)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeFamily  $employeeFamily
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeFamily $employeeFamily)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeFamily  $employeeFamily
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeFamily $employeeFamily)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeFamily  $employeeFamily
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeFamily $employeeFamily)
    {
        //
    }
}
